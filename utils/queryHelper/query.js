// if (req.query.sDescription) {
//     const result = await blogModel.aggregate([
//         { $match: { sDescription: new RegExp(req.query.sDescription, "i") } },
//         {
//             $facet: {
//                 "medataData": [{ $count: "total" }, { $addFields: { page: { $ceil: { $divide: ["$total", parseInt(req.query.limit || 5)] } } } }],
//                 "data": [{ $skip: ((page * limit) - limit) }, { $limit: limit }]
//             }
//         }
//     ]);
//     if (!result[0].data.length) {
//         return res.status(200).json({ result: "no record found" });
//     }

//     return res.status(200).json({ result });
// }

const helperQuery = {}


helperQuery.pagination = (query) => {
    const limit = parseInt(query?.limit || 5)
    const page = parseInt(query?.page || 1)
    return ({
        $facet: {
            "medataData": [{ $count: "total" }, { $addFields: { page: { $ceil: { $divide: ["$total", limit] } } } }],
            "data": [{ $skip: ((page * limit) - limit) }, { $limit: limit }]
        }

    })
}

helperQuery.published = () => {

    return ({
        $set:
        {
            isPublished: "true"

        }
    })
}

helperQuery.notPublished = () => {

    return ({
        $set:
        {
            isPublished: "false"

        }
    })
}

helperQuery.countLikes = () => {

    return ({
        $set:
        {

            count:
                { $size: "$aLikes" },

        }
    })
}

helperQuery.order = (query) => {
    const order = parseInt(query?.order || -1)
    return ({
        $sort:
            { "count": order }

    })
}

module.exports = helperQuery


 // if (req.query.search) {
        //     const result = await blogModel.aggregate([

        //         { $match: { [req.query.search]: new RegExp(req.query.searchBy, "i") } },
        //         {
        //             $facet: {
        //                 "medataData": [{ $count: "total" }, { $addFields: { page: { $ceil: { $divide: ["$total", parseInt(req.query.limit || 5)] } } } }],
        //                 "data": [{ $skip: ((page * limit) - limit) }, { $limit: limit }]
        //             }

        //         }
        //     ]);
        //     if (!result[0].data.length) {
        //         return res.status(200).json({ result: "no record found" });
        //     }
        //     return res.status(200).json({ result });
        // }


        // if (req.query.published == "false") {

        //     const date = new Date();
        //     const result = await blogModel.aggregate([
        //         {
        //             $match:
        //             {
        //                 $expr:
        //                 {
        //                     $gte:
        //                         ["$dPublishByDate", date]
        //                 }
        //             }
        //         },
        //         {
        //             $set:
        //             {
        //                 count:
        //                     { $size: "$aLikes" },
        //                 isPublished: "false"

        //             }
        //         },
        //         {
        //             $sort:
        //                 { "count": order }

        //         },
        //         {
        //             $facet: {
        //                 "medataData": [{ $count: "total" }, { $addFields: { page: { $ceil: { $divide: ["$total", limit] } } } }],
        //                 "data": [{ $skip: (page * limit) - limit }, { $limit: limit }]
        //             }
        //         }
        //     ]);
        //     if (!result[0].data.length) {
        //         return res.status(200).json({ result: "no record found" });
        //     }
        //     return res.status(200).json({ result });
        // }

        // if (req.query.published == "true") {
        //     const date = new Date();
        //     const result = await blogModel.aggregate([
        //         {
        //             $match:
        //             {
        //                 $expr:
        //                 {
        //                     $lte:
        //                         ["$dPublishByDate", date]
        //                 }
        //             }
        //         },
        //         {
        //             $set:
        //             {
        //                 count:
        //                     { $size: "$aLikes" },
        //                 isPublished: "true"

        //             }
        //         },
        //         {
        //             $sort:
        //                 { "count": order }

        //         }, {
        //             $facet: {
        //                 "medataData": [{ $count: "total" }, { $addFields: { page: { $ceil: { $divide: ["$total", limit] } } } }],
        //                 "data": [{ $skip: (page * limit) - limit }, { $limit: limit }]
        //             }
        //         }
        //     ]);
        //     if (!result[0].data.length) {
        //         return res.status(200).json({ result: "no record found" });
        //     }
        //     return res.status(200).json({ result });
        // }

        // if (req.query.createdRange == "true") {

        //     if (startDate > lastDate) {
        //         throw new Error("statdate should be lesser to lastdate");
        //     }


        //     const result = await blogModel.aggregate([
        //         { $match: { [req.query.search]: new RegExp(req.query.searchBy, "i") } },
        //         {
        //             $match:
        //             {
        //                 createdAt: {
        //                     $gt: startDate,
        //                     $lt: lastDate
        //                 }
        //             }
        //         },
        //         {
        //             $set:
        //             {
        //                 count:
        //                     { $size: "$aLikes" },
        //                 isPublished: "true"

        //             }
        //         },
        //         {
        //             $sort:
        //                 { "count": order }

        //         }, {
        //             $facet: {
        //                 "medataData": [{ $count: "total" }, { $addFields: { page: { $ceil: { $divide: ["$total", limit] } } } }],
        //                 "data": [{ $skip: (page * limit) - limit }, { $limit: limit }]
        //             }
        //         }
        //     ]);
        //     if (!result[0].data.length) {
        //         return res.status(200).json({ result: "no record found" });
        //     }
        //     return res.status(200).json({ result });

        // }


        // if (req.query.publishedRange == "true") {

        //     if (startDate > lastDate) {
        //         throw new Error("statdate should be lesser to lastdate");
        //     }

        //     const result = await blogModel.aggregate([
        //         {
        //             $match:
        //             {
        //                 dPublishByDate: {
        //                     $gt: startDate,
        //                     $lt: lastDate
        //                 }
        //             }
        //         },
        //         {
        //             $set:
        //             {
        //                 count:
        //                     { $size: "$aLikes" },
        //                 isPublished: "true"

        //             }
        //         },
        //         {
        //             $sort:
        //                 { "count": order }

        //         }, {
        //             $facet: {
        //                 "medataData": [{ $count: "total" }, { $addFields: { page: { $ceil: { $divide: ["$total", limit] } } } }],
        //                 "data": [{ $skip: (page * limit) - limit }, { $limit: limit }]
        //             }
        //         }
        //     ]);
        //     if (!result[0].data.length) {
        //         return res.status(200).json({ result: "no record found" });
        //     }
        //     return res.status(200).json({ result });

        // }


        // const result = await blogModel.aggregate([
        //     {
        //         $match: {}
        //     },
        //     {
        //         $set:
        //         {
        //             count:
        //                 { $size: "$aLikes" }

        //         }
        //     },
        //     {
        //         $sort:
        //             { "count": order }

        //     },
        //     {
        //         $facet: {
        //             "medataData": [{ $count: "total" }, { $addFields: { page: { $ceil: { $divide: ["$total", limit] } } } }],
        //             "data": [{ $skip: (page * limit) - limit }, { $limit: limit }]
        //         }
        //     }
        // ]);

