const cloudinary = require("cloudinary").v2
const fs = require("fs")
const util = require("util")
const unlinkFile = util.promisify(fs.unlink)



cloudinary.config({
    cloud_name: process.env.CLOUDINARY_CLOUDNAME,
    api_key: process.env.CLOUDINARY_API_KEY,
    api_secret: process.env.CLOUDINARY_API_SECRET
})


async function uploadfile(path) {
    const result = await cloudinary.uploader.upload(path, {
        folder: "userAvtar",
    })
    await unlinkFile(path)
    return result
}


async function deleteFile(url) {
    const result = await cloudinary.uploader.destroy(url)
    return result
}


async function deleteOneFile(avatar) {
    let split = avatar.split("/")
    let folder = split[split.length - 2]
    let file = split[split.length - 1].split(".")[0]
    let file_path = folder + "/" + file
    console.log(file_path)
    const result = await cloudinary.uploader.destroy(file_path)
    return result
}


async function deleteManyFile(blogs) {

    let data = blogs.map((e) => {
        let split = e.sCoverPic.split("/")
        let folder = split[split.length - 2]
        let file = split[split.length - 1].split(".")[0]
        let file_path = folder + "/" + file
        return file_path
    })

    const result = await cloudinary.api.delete_resources(data)
    return result
}

async function unlink(path) {
    await unlinkFile(path)
}


module.exports = { uploadfile, unlink, deleteFile, deleteManyFile, deleteOneFile }




