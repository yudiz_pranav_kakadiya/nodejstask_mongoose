const validator = {}
const fs = require("fs")
const util = require("util")
const unlinkFile = util.promisify(fs.unlink)

validator.signUp = async (req, res, next) => {
    if (!(req.file.mimetype === "image/jpeg" || req.file.mimetype === "image/jpg" || req.file.mimetype === "image/png")) {
        await unlinkFile(req.file.path)
        return res.status(400).json({ message: "Image formate should be jpeg,jpg or png" })
    }
    next()
}


validator.createBlog = async (req, res, next) => {
    if (!(req.file.mimetype === "image/jpeg" || req.file.mimetype === "image/jpg" || req.file.mimetype === "image/png")) {
        await unlinkFile(req.file.path)
        return res.status(400).json({ message: "Image formate should be jpeg,jpg or png" })
    }
    next()
}

validator.editProfile = async (req, res, next) => {
    if (req.file) {
        if (!(req.file.mimetype === "image/jpeg" || req.file.mimetype === "image/jpg" || req.file.mimetype === "image/png")) {
            await unlinkFile(req.file.path)
            return res.status(400).json({ message: "Image formate should be jpeg,jpg or png" })
        }
    }
    next()
}

validator.editBlog = async (req, res, next) => {
    if (req.file) {
        if (!(req.file.mimetype === "image/jpeg" || req.file.mimetype === "image/jpg" || req.file.mimetype === "image/png")) {
            await unlinkFile(req.file.path)
            return res.status(400).json({ message: "Image formate should be jpeg,jpg or png" })
        }
    }
    next()
}


module.exports = validator
