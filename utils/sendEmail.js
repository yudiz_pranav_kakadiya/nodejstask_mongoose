const sgMail = require("@sendgrid/mail")
sgMail.setApiKey(process.env.sendGrid_Api_key)

const sendEmail = (receiver, source, subject, content) => {
    try {
        const data = {
            to: receiver,
            from: source,
            subject,
            html: content,
        }
        return sgMail.send(data)
    } catch (error) {
        return new Error(error.message)
    }
}

module.exports = sendEmail
