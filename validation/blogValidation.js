const validator = {}
const fs = require("fs")
const util = require("util")
const unlinkFile = util.promisify(fs.unlink)
const blogModel = require("../models/blogSchema")



validator.checkCreateBlog = async (req, res, next) => {

    try {

        const { sTitle, sDescription, sPublishByDate } = req.body
        const dateRegex = /^\d{2}\/\d{2}\/\d{4}$/
        if (!sTitle) {
            throw new Error("Title required")
        }

        if (!sDescription) {
            throw new Error("sDescription required")
        }

        if (dateRegex.test(sPublishByDate) == false) {
            throw new Error("date in formate mm/dd/yyyy required")
        }

        // let date = new Date(sPublishByDate)
        // let curretDate = new Date()



        // if (curretDate > date) {
        //     throw new Error("date should be grater or equal to current date")
        // }

        if (!req.file) {
            throw new Error("image required")
        }
        if (!(req.file.mimetype === "image/jpeg" || req.file.mimetype === "image/jpg" || req.file.mimetype === "image/png")) {
            await unlinkFile(req.file.path)
            throw new Error("Image formate should be jpeg,jpg or png")
        }
        next()

    } catch (error) {
        if (req.file) {
            await unlinkFile(req.file.path)
        }
        return res.status(400).json({ message: error.message })
    }
}



validator.checkEditBlog = async (req, res, next) => {

    try {

        const { sBlogId, sPublishByDate } = req.body

        if (!sBlogId) {
            throw new Error("sBlogId required")
        }
        const dateRegex = /^\d{2}\/\d{2}\/\d{4}$/

        if (sPublishByDate) {
            if (dateRegex.test(sPublishByDate) == false) {
                throw new Error("date in formate mm/dd/yyyy required")
            }
        }

        if (req.file) {
            if (!(req.file.mimetype === "image/jpeg" || req.file.mimetype === "image/jpg" || req.file.mimetype === "image/png")) {
                throw new Error("Image formate should be jpeg,jpg or png")
            }
        }
        next()

    } catch (error) {
        if (req.file) {
            await unlinkFile(req.file.path)
        }
        return res.status(400).json({ message: error.message })
    }
}

validator.checkGetBlog = async (req, res, next) => {
    try {
        const { sBlogId } = req.body

        if (!sBlogId) {
            throw new Error("blogId required")
        }
        next()
    }
    catch (error) {
        return res.status(400).json({ message: error.message })
    }
}

validator.checkLikes = async (req, res, next) => {
    try {
        const { sBlogId } = req.body

        if (!sBlogId) {
            throw new Error("blogId required")
        }
        next()
    }
    catch (error) {
        return res.status(400).json({ message: error.message })
    }
}


validator.checkUnpublish = async (req, res, next) => {
    try {
        const { sBlogId } = req.body

        if (!sBlogId) {
            throw new Error("blogId required")
        }
        next()
    }
    catch (error) {
        return res.status(400).json({ message: error.message })
    }
}




module.exports = validator