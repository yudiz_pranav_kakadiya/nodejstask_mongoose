const validator = {}
const fs = require("fs")
const util = require("util")
const unlinkFile = util.promisify(fs.unlink)
const userModel = require("../models/userSchema")

const input_match = ["sEmail",
    "sPassword",
    "sGender",
    "sMobile",
    "sUserName",
    "sText",
    "image",
    "sCurrentPassword",
    "sNewPassword",
    "sConfirmPassword"
]


const matchInputField = async (data) => {

    let key = Object.keys(data)

    let match = key.filter((value) => {
        return !input_match.includes(value)
    })

    if (match.length > 0) {
        return -1
    }

    return 1

}


validator.checkLogIn = async (req, res, next) => {

    try {
        const { sEmail, sPassword } = req.body


        if (await matchInputField(req.body) == -1) {
            throw new Error("there is one extra malicious field in to the input")
        }

        const emailregex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/

        if (!sEmail) {
            throw new Error("email required")
        }
        if (emailregex.test(sEmail) == false) {
            throw new Error("email with formate")
        }
        if (!sPassword) {
            throw new Error("password required")
        }
        if (sPassword.length < 8) {
            throw new Error("password at least 8 character long")
        }

        next()
    }
    catch (error) {
        return res.status(400).json({ message: error.message })
    }
}
validator.checkPassword = async (req, res, next) => {
    try {
        const { sCurrentPassword, sNewPassword, sConfirmPassword } = req.body

        if (await matchInputField(req.body) == -1) {
            throw new Error("there is one extra malicious field in to the input")
        }

        if (!sCurrentPassword) {
            throw new Error("CurrentPassword required")
        }
        if (sCurrentPassword.length < 8) {
            throw new Error("CurrentPassword at least 8 character long")
        }
        if (!sNewPassword) {
            throw new Error("CurrentPassword required")
        }
        if (sNewPassword.length < 8) {
            throw new Error("CurrentPassword at least 8 character long")
        }
        if (sNewPassword != sConfirmPassword) {
            throw new Error("password should be match")
        }
        next()
    }
    catch (error) {
        return res.status(400).json({ message: error.message })
    }
}
validator.checkForgotPassword = async (req, res, next) => {
    try {
        const { sEmail } = req.body
        const emailregex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/

        if (!sEmail) {
            throw new Error("email required")
        }
        if (emailregex.test(sEmail) == false) {
            throw new Error("email with formate")
        }

        if (!await userModel.findOne({ sEmail: sEmail })) {
            throw new Error("Email not registered")
        }

        next()
    }
    catch (error) {
        return res.status(400).json({ message: error.message })
    }
}

validator.checkResetPassword = async (req, res, next) => {
    try {
        const { sPassword } = req.body
        if (!sPassword) {
            throw new Error("CurrentPassword required")
        }
        if (sPassword.length < 8) {
            throw new Error("CurrentPassword at least 8 character long")
        }
        next()
    }
    catch (error) {
        return res.status(400).json({ message: error.message })
    }
}

validator.checkSignUp = async (req, res, next) => {

    try {
        const { sEmail, sPassword, sGender, sMobile, sUserName } = req.body

        let roles = ["ADMIN", "USER"]
        let gender = ["Male", "Female", "Other"]
        const mobileRegex = /^([0|\+[0-9]{1,5})?([6-9][0-9]{9})$/
        const emailregex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/

        if (!sEmail) {
            throw new Error("email required")
        }
        if (!sUserName) {
            throw new Error("username required")
        }
        if (emailregex.test(sEmail) == false) {
            throw new Error("email with formate")
        }
        if (!sPassword) {
            throw new Error("password required")
        }
        if (sPassword.length < 8) {
            throw new Error("password at least 8 character long")

        }
        if (!sGender) {
            throw new Error("gender required")
        }
        if (!gender.includes(sGender)) {
            throw new Error("select from 'Male','Female','Other'")
        }
        if (!sMobile) {
            throw new Error("mobile number required")
        }
        if (mobileRegex.test(sMobile) == false) {
            throw new Error("mobile number not in formate")
        }
        if (await userModel.findOne({ sEmail: sEmail })) {
            throw new Error("Email already register")
        }


        if (!req.file) {
            throw new Error("image required")

        }
        if (!(req.file.mimetype === "image/jpeg" || req.file.mimetype === "image/jpg" || req.file.mimetype === "image/png")) {
            await unlinkFile(req.file.path)
            throw new Error("Image formate should be jpeg,jpg or png")
        }
        next()

    } catch (error) {
        if (req.file) {
            await unlinkFile(req.file.path)
        }
        return res.status(400).json({ message: error.message })
    }
}


validator.checkEditProfile = async (req, res, next) => {

    try {
        let gender = ["Male", "Female", "Other"]
        const { sMobile = req.user.sMobile, sGender = req.user.sGender } = req.body

        const mobileRegex = /^([0|\+[0-9]{1,5})?([6-9][0-9]{9})$/
        if (!sGender) {
            throw new Error("sGender required")
        }

        if (!gender.includes(sGender)) {
            throw new Error("select from 'Male','Female','Other'")
        }
        if (!sMobile) {
            throw new Error("mobile number required")
        }
        if (mobileRegex.test(sMobile) == false) {
            throw new Error("mobile number not in formate")
        }

        if (req.file) {
            if (!(req.file.mimetype === "image/jpeg" || req.file.mimetype === "image/jpg" || req.file.mimetype === "image/png")) {
                await unlinkFile(req.file.path)
                throw new Error("Image formate should be jpeg,jpg or png")
            }
        }
        next()

    } catch (error) {
        if (req.file) {
            await unlinkFile(req.file.path)
        }
        return res.status(400).json({ message: error.message })
    }
}


module.exports = validator