const validator = {}
const fs = require("fs")
const util = require("util")
const unlinkFile = util.promisify(fs.unlink)
const userModel = require("../models/userSchema")

// const input_match = ['sEmail','sPassword','sGender','sMobile','sUserName','sText','image'];



validator.checkSignUp = async (req, res, next) => {

    try {
        const { sEmail, sPassword, sGender, sMobile, sUserName } = req.body

        let roles = ["ADMIN", "USER"]
        let gender = ["Male", "Female", "Other"]
        const mobileRegex = /^([0|\+[0-9]{1,5})?([6-9][0-9]{9})$/
        const emailregex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/

        if (!sEmail) {
            throw new Error("email required")
        }
        if (!sUserName) {
            throw new Error("username required")
        }
        if (emailregex.test(sEmail) == false) {
            throw new Error("email with formate")
        }
        if (!sPassword) {
            throw new Error("password required")
        }
        if (sPassword.length < 8) {
            throw new Error("password at least 8 character long")

        }
        if (!sGender) {
            throw new Error("gender required")
        }
        if (!gender.includes(sGender)) {
            throw new Error("select from 'Male','Female','Other'")
        }
        if (!sMobile) {
            throw new Error("mobile number required")
        }
        if (mobileRegex.test(sMobile) == false) {
            throw new Error("mobile number not in formate")
        }
        if (await userModel.findOne({ sEmail: sEmail })) {
            throw new Error("Email already register")
        }


        if (!req.file) {
            throw new Error("image required")

        }
        if (!(req.file.mimetype === "image/jpeg" || req.file.mimetype === "image/jpg" || req.file.mimetype === "image/png")) {
            await unlinkFile(req.file.path)
            throw new Error("Image formate should be jpeg,jpg or png")
        }
        next()

    } catch (error) {
        if (req.file) {
            await unlinkFile(req.file.path)
        }
        return res.status(400).json({ message: error.message })
    }
}
validator.checkUserDelete = async (req, res, next) => {
    try {
        const { sUserId } = req.body
        const { _id } = req.user

        if (!sUserId) {
            throw new Error("iUserId required")
        }
        let match = await userModel.findOne({ _id: sUserId })

        if (!match) {
            throw new Error("user Not exists")
        }
        if (match.aRole.includes("ADMIN")) {
            throw new Error("u can not delete admin")
        }
        if (sUserId === _id) {
            throw new Error("not can not delete ur self")
        }
        next()
    }
    catch (error) {
        return res.status(400).json({ message: error.message })
    }
}
validator.checkUserId = async (req, res, next) => {
    try {
        const { sUserId } = req.body

        if (!sUserId) {
            throw new Error("iUserId required")
        }
        let match = await userModel.findOne({ _id: sUserId })

        if (!match) {
            throw new Error("user Not exists")
        }
        next()
    }
    catch (error) {
        return res.status(400).json({ message: error.message })
    }
}



validator.checkMonthWise = async (req, res, next) => {
    try {
        const { sMonth } = req.body

        if (sMonth > 12) {
            throw new Error("month should be less then 13")
        }
        if (sMonth <= 0) {
            throw new Error("month should be grater or equal to 1")
        }
        next()

    } catch (error) {
        return res.status(400).json({ message: error.message })
    }
}



module.exports = validator