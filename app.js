require("dotenv").config()
const express = require("express")
const routes = require("./routes/route")
const bodyParser = require("body-parser")

const app = express()

require("./database/connection")

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.use("/api/v1", routes)

app.listen(process.env.PORT || 5055, () => console.log("backend server is running on port " + 5055))
