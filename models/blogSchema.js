const mongoose = require("mongoose")

const blogSchema = new mongoose.Schema(
    {
        iUserId: {
            type: mongoose.Schema.Types.ObjectId,
            require: true,
            ref: "users"
        },
        sUserName: {
            type: String,
            require: true,
            trim: true,
        },
        sTitle: {
            type: String,
            require: true,
            trim: true,
        },
        sDescription: {
            type: String,
            require: true,
            trim: true,
        },
        dPublishByDate: {
            type: Date,
        },
        aLikes: [
            { type: String }
        ],
        sCoverPic: { type: String, required: true },
    },
    { timestamps: true }
)

// blogSchema.index({ userName: "text", sTitle: "text" });

const blogModel = mongoose.model("blogs", blogSchema)

module.exports = blogModel
