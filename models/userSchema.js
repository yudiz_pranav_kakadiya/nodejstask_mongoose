const mongoose = require("mongoose")

const userSchema = new mongoose.Schema(
    {
        sUserName: {
            type: String,
            require: true,
            trim: true,
        },
        sMobile: {
            type: String,
            require: true,
            trim: true,
        },
        sGender: {
            type: String,
            require: true,
            trim: true,
        },
        aRole: [
            {
                type: String,
                enum: ["USER", "ADMIN"],
                default: "USER",
            },
        ],
        sProfilePic: { type: String },
        sEmail: {
            type: String,
            require: true,
            trim: true,
            unique: true,
        },
        sPassword: {
            type: String,
            require: true,
            trim: true,
        },
        aTokens: [
            {
                token: {
                    type: String,
                },
            },
        ],
    },
    { timestamps: true }
)

const userModel = mongoose.model("users", userSchema)

module.exports = userModel
