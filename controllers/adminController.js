const controller = {}

const { hashPassword } = require("../utils/bcrypt/hashPassword")

const { uploadfile, unlink, deleteManyFile, deleteOneFile } = require("../utils/uploadCloudinary/cloudinary")

const userModel = require("../models/userSchema")
const blogModel = require("../models/blogSchema")

const helperQuery = require("../utils/queryHelper/query")

controller.createUser = async (req, res) => {

    const { sUserName, sEmail, sPassword, sRole, sGender, sMobile } = req.body
    try {
        if (await userModel.findOne({ sEmail: sEmail })) return res.status(400).json({ message: "sEmail already register" })
        const result = await uploadfile(req.file.path)
        const hashedPass = await hashPassword(sPassword)
        const userData = { sUserName: sUserName, sEmail: sEmail, sPassword: hashedPass, aRole: sRole, sGender: sGender, sMobile: sMobile, sProfilePic: result?.url || " " }
        const user = new userModel(userData)
        await user.save()
        return res.status(201).json({ user: user })
    } catch (error) {
        await unlink(req.file.path)
        return res.status(500).json({ message: error.message })
    }
}


controller.listUser = async (req, res) => {
    try {
        const user = await userModel.find({}, { sUserName: 1, sEmail: 1, aRole: 1 })
        return res.status(200).json({ message: user })
    } catch (error) {
        return res.status(500).json({ message: error.message })
    }
}




controller.deleteUser = async (req, res) => {
    const { sUserId } = req.body
    try {

        const user = await userModel.findByIdAndDelete({ _id: sUserId })
        await deleteOneFile(user.sProfilePic)

        await blogModel.find({ iUserId: sUserId })
        await deleteManyFile(blogs)

        return res.status(200).json({ message: "user delete successfully" })
    } catch (error) {
        return res.status(500).json({ message: error.message })
    }
}


controller.getProfileOfUser = async (req, res) => {
    const { sUserId } = req.body
    try {
        const user = await userModel.findById({ _id: sUserId })
        return res.status(200).json({ message: user })
    } catch (error) {
        return res.status(500).json({ message: error.message })
    }
}




// controller.showBlogByUserWithLikeSorting = async (req, res) => {
//     const date = new Date();
//     try {
//         const result = await blogModel.aggregate([
//             {
//                 $facet: {
//                     published: [{ $set: { count: { $size: "$aLikes" } } }, { $match: { $and: [{ "dPublishByDate": { $ne: null } }, { $expr: { $lt: ["$dPublishByDate", date] } }] } }, { $group: { _id: "$iUserId", blogs: { $push: "$$ROOT" } } }, { $sort: { "blogs.count": -1 } }],
//                     notPublished: [{ $set: { count: { $size: "$aLikes" } } }, { $match: { $and: [{ "dPublishByDate": { $ne: null } }, { $expr: { $gt: ["$dPublishByDate", date] } }] } }, { $group: { _id: "$iUserId", blogs: { $push: "$$ROOT" } } }, { $sort: { "blogs.count": -1 } }],
//                     haveNotPublished: [{ $match: { "dPublishByDate": { $eq: null } } }]
//                 }
//             }
//         ]);
//         return res.status(200).json({ result });
//     } catch (error) {
//         return res.status(500).json({ message: "Error" + error });
//     }
// };




//date filter

controller.dashBoard = async (req, res) => {

    let queryPipline = []

    try {

        let startDate = req.query?.sDate ? new Date(req.query.sDate) : new Date()
        let lastDate = req.query?.lDate ? new Date(req.query.lDate) : new Date()
        const date = new Date()

        let daysRange = new Date()
        daysRange.setDate(startDate.getDate() + parseInt(req.query.days))






        if (req.query.search) {
            queryPipline.push({ $match: { [req.query.search]: new RegExp(req.query.searchBy, "i") } })
        }

        if (Boolean(req.query.notPublished)) {

            queryPipline.push({
                $match:
                {
                    $expr:
                    {
                        $gte:
                            ["$dPublishByDate", date]
                    }
                }
            }, helperQuery.notPublished())
        }
        if (Boolean(req.query.published)) {
            queryPipline.push({
                $match:
                {
                    $expr:
                    {
                        $lte:
                            ["$dPublishByDate", date]
                    }
                }
            }, helperQuery.published())
        }

        if (req.query.range) {
            queryPipline.push({
                $match:
                {
                    [req.query.range]: {
                        $gt: startDate,
                        $lt: lastDate
                    }
                }
            })
        }

        if (req.query.days) {

            if (parseInt(req.query.days) >= 0) {
                queryPipline.push({
                    $match:
                    {
                        [req.query.field]: {
                            $gt: startDate,
                            $lt: daysRange
                        }
                    }
                })
            }
            if (parseInt(req.query.days) < 0) {
                queryPipline.push({
                    $match:
                    {
                        [req.query.field]: {
                            $gt: daysRange,
                            $lt: startDate
                        }
                    }
                })
            }
        }

        queryPipline.push(helperQuery.countLikes(), helperQuery.order(req.query), helperQuery.pagination(req.query))



        const result = await blogModel.aggregate(queryPipline)


        if (!result[0].data.length) {
            return res.status(200).json({ result: "no record found" })
        }


        return res.status(200).json({ result })

    } catch (error) {

        return res.status(500).json({ message: "Error" + error })
    }
}




module.exports = controller

