const controller = {}
const jwt = require("jsonwebtoken")
const sendEmail = require("../utils/sendEmail")

const { hashPassword, comparePassword, } = require("../utils/bcrypt/hashPassword")
const generateToken = require("../utils/tokenGenerate/jwtToken")

const { uploadfile, unlink } = require("../utils/uploadCloudinary/cloudinary")

const userModel = require("../models/userSchema")

const formate = require("../utils/mailFormate")
const blogModel = require("../models/blogSchema")

controller.signUp = async (req, res) => {

    const { sUserName, sEmail, sPassword, sRole, sGender, sMobile } = req.body
    try {
        if (await userModel.findOne({ sEmail: sEmail })) return res.status(400).json({ message: "sEmail already register" })
        const result = await uploadfile(req.file.path)

        const hashedPass = await hashPassword(sPassword)
        const userData = { sUserName: sUserName, sEmail: sEmail, sPassword: hashedPass, aRole: sRole || "USER", sGender: sGender, sMobile: sMobile, sProfilePic: result?.url || " " }
        const user = new userModel(userData)
        const token = await generateToken(user._id)
        user.aTokens.push({ token })
        await user.save()
        return res.status(201).json({ user: user, token: token })
    } catch (error) {
        await unlink(req.file.path)
        return res.status(500).json({ message: error.message })
    }
}

controller.logIn = async (req, res) => {
    const { sEmail, sPassword } = req.body
    try {
        const user = await userModel.findOne({ sEmail: sEmail })
        if (!user) return res.status(400).json({ message: "User not exits" })
        const isMatch = await comparePassword(sPassword, user.sPassword)
        if (!isMatch) return res.status(400).json({ message: "sPassword not match" })
        const token = await generateToken(user._id)
        if (user.aTokens.length > 5) user.aTokens.shift()
        user.aTokens.push({ token })
        await user.save()
        return res.status(200).json({ message: "Successfully login", token })
    } catch (error) {
        return res.status(500).json({ message: "Error" })
    }
}

controller.changePassword = async (req, res) => {
    const { sCurrentPassword, sNewPassword } = req.body
    const { _id, sPassword } = req.user
    try {
        const isMatch = await comparePassword(sCurrentPassword, sPassword)
        if (!isMatch) return res.status(400).json({ message: "sPassword not match" })
        const hashedPass = await hashPassword(sNewPassword)
        await userModel.updateOne({ _id }, { sPassword: hashedPass })
        return res.status(200).json({ message: "sPassword changed" })
    } catch (error) {
        return res.status(500).json({ message: "Error :" + error.message })
    }
}

controller.getProfile = async (req, res) => {
    const { _id } = req.user
    try {
        const user = await userModel.find({ _id: _id })
        return res.status(200).json({ message: user })
    } catch (error) {
        return res.status(500).json({ message: error.message })
    }
}


controller.getMyBlogs = async (req, res) => {
    const { _id } = req.user
    try {
        const user = await blogModel.find({ iUserId: _id })
        return res.status(200).json({ message: user })
    } catch (error) {
        return res.status(500).json({ message: error.message })
    }
}


controller.logout = async (req, res) => {
    const { _id } = req.user
    try {
        req.user.aTokens = req.user.aTokens.filter((token) => {
            return token.token !== req.token
        })
        await userModel.updateOne({ _id }, { aTokens: req.user.aTokens })
        return res.status(200).json({ message: "Successfully logout" })
    } catch (error) {
        return res.status(500).json({ message: error.message })
    }
}


controller.editProfile = async (req, res) => {
    const { sMobile, sGender } = req.body
    const { _id } = req.user
    try {
        const user = await userModel.findById({ _id: _id })

        let result = user.sProfilePic

        if (req.file) {
            result = await uploadfile(req.file.path)
        }




        await userModel.updateOne({ _id: _id }, { sMobile, sGender, sProfilePic: result?.url || req.user.sProfilePic })
        return res.status(201).json({ message: "Successfully updated" })
    } catch (error) {
        return res.status(500).json({ message: "Error :" + error.message })
    }
}

controller.forgotPassword = async (req, res) => {
    try {
        const { sEmail } = req.body
        const token = jwt.sign({ _id: sEmail }, process.env.ACCESS_TOKEN_SECRET, {
            expiresIn: "1h",
        })

        const link = `${req.protocol}:${req.get("host")}/api/v1/users/resetPassword/${token}`
        await sendEmail(sEmail, "pkakadiya274@rku.ac.in", "pasword reset", formate(link)
        )
        return res.status(200).send({ message: "sPassword reset has been successfully sent" })
    } catch (e) {
        return res.status(400).json({ message: e.message })
    }
}



controller.resetPassword = async (req, res) => {
    try {
        const { sPassword } = req.body
        const { token } = req.params
        const decoded = await jwt.verify(token, process.env.ACCESS_TOKEN_SECRET)
        const user = await userModel.findOne({ sEmail: decoded._id })
        const hashedPass = await hashPassword(sPassword)
        user.sPassword = hashedPass
        await user.save()
        return res.status(200).json({ message: "sPassword Updated" })
    } catch (e) {
        return res.status(400).json({ message: e.message })
    }
}


module.exports = controller


