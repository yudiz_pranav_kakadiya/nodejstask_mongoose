const controller = {}

const { uploadfile, unlink } = require("../utils/uploadCloudinary/cloudinary")

const blogModel = require("../models/blogSchema")

controller.createBlog = async (req, res) => {

    const { sTitle, sDescription, sPublishByDate } = req.body
    const { sUserName, _id } = req.user
    try {
        const result = await uploadfile(req.file.path)
        const blogData = { sUserName, iUserId: _id, sTitle: sTitle, sDescription: sDescription, dPublishByDate: sPublishByDate || new Date(), sCoverPic: result?.url || " " }
        const blog = new blogModel(blogData)
        await blog.save()
        return res.status(201).json({ blog: blog })
    } catch (error) {
        if (req.file) {
            await unlink(req.file.path)
        }
        return res.status(500).json({ message: error.message })
    }
}

controller.getBlog = async (req, res) => {
    const { _id } = req.user
    try {
        const blogs = await blogModel.find({ iUserId: _id })
        return res.status(200).json({ blogs })
    } catch (error) {
        return res.status(500).json({ message: "Error" + error })
    }
}


controller.getAllBlog = async (req, res) => {
    try {
        let date = new Date()
        const blogs = await blogModel.find({ $and: [{ dPublishByDate: { $lte: date } }, { $dPublishByDate: { $exists: true, $not: null } }] })
        return res.status(200).json({ blogs })
    } catch (error) {
        return res.status(500).json({ message: "Error" + error })
    }
}


controller.likes = async (req, res) => {
    const { sBlogId } = req.body
    const { _id } = req.user
    const date = new Date()
    try {
        let blog = await blogModel.findOne({ $and: [{ _id: sBlogId }, { $exists: { dPublishByDate: true } }, { dPublishByDate: { $lte: date } }] })
        if (!blog) {
            return res.status(200).json({ id: "blog is not published till now " })
        }
        const isLike = blog.aLikes.indexOf(_id)
        if (isLike == -1) {
            blog.aLikes.addToSet(_id)
            await blog.save()
            return res.status(200).json({ id: "isLike" })
        } else {
            blog.aLikes.pull(_id)
            await blog.save()
            return res.status(200).json({ id: "disLike" })
        }
    } catch (error) {
        return res.status(500).json({ message: "Error" + error })
    }
}

controller.editBlog = async (req, res) => {

    const { sTitle, sDescription, sPublishByDate, sBlogId } = req.body
    const { _id } = req.user
    try {
        let match = await blogModel.findOne({ _id: sBlogId, iUserId: _id })
        if (!match) return res.status(201).json({ user: "product not exist" })
        if (req.file) {
            var result = await uploadfile(req.file.path)
        }
        await blogModel.updateOne({ _id: sBlogId, iUserId: _id }, { sTitle: sTitle, sDescription: sDescription, dPublishByDate: sPublishByDate, sCoverPic: result?.url })
        return res.status(201).json({ blog: "blog" })
    } catch (error) {
        return res.status(500).json({ message: error.message })
    }
}


controller.makeBlogUnpublish = async (req, res) => {

    const { sBlogId } = req.body
    const { _id } = req.user
    try {
        let match = await blogModel.findOne({ _id: sBlogId, iUserId: _id })
        if (!match) return res.status(201).json({ user: "product not exist" })

        await blogModel.updateOne({ _id: sBlogId, iUserId: _id }, { $unset: { dPublishByDate: 1 } })
        return res.status(201).json({ blog: "blog" })
    } catch (error) {
        return res.status(500).json({ message: error.message })
    }
}


controller.listBlog = async (req, res) => {
    try {
        const blogs = await blogModel.find({})
        return res.status(200).json({ message: blogs })
    } catch (error) {
        return res.status(500).json({ message: "Error" + error })
    }
}


module.exports = controller