const jwt = require("jsonwebtoken")
const middleware = {}
const userModel = require("../models/userSchema")

middleware.authorizationToken = (req, res, next) => {
    const token = req.headers.authorization

    if (!token) return res.status(401).json({ message: "error" })
    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, async (err, data) => {
        if (err) return res.status(401).json({ message: err })

        const user = await userModel.findOne({
            _id: data.id,
            "tokens.token": token,
        })
        req.token = token
        if (!user) return res.status(401).json({ message: "token invalid" })
        req.user = user

        next()
    })
}

middleware.roleCheck = (req, res, next) => {
    const { aRole } = req.user
    try {
        if (!aRole.includes("ADMIN")) return res.status(401).json({ message: "unauthorize" })
        next()
    } catch (error) {
        return res.status(401).json({ message: "ur unouthorized user" })
    }
}






module.exports = middleware