const mongoose = require("mongoose")

mongoose.connect(process.env.MONGO_URL)
mongoose.connection
    .once("open", () => console.log("connected to MongoDB!"))
    .on("error", err => console.error("connecting to MongoDB " + err))