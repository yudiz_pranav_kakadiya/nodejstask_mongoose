//require routr
const router = require("express").Router()
const users = require("./userRoutes")
const blogs = require("./blogRoutes")
const admin = require("./adminRoutes")


//user route
router.use("/users", users)
router.use("/blogs", blogs)
router.use("/admin", admin)



//handling other route
router.all("*", (req, res) => {
    res.status(404).json({ message: "No user Route Found" })
})

module.exports = router