const router = require("express").Router()
const controller = require("../controllers/userController")

const multer = require("multer")
const upload = multer({ dest: "photo/" })


// const validation = require("../utils/validation")

const middleware = require("../middleware/permission")

const userValidation = require("../validation/userValidation")

//signUp
router.post("/signUp", upload.single("image"), userValidation.checkSignUp, controller.signUp)

router.post("/login", userValidation.checkLogIn, controller.logIn)

router.get("/logout", middleware.authorizationToken, controller.logout)

router.post("/changePassword", middleware.authorizationToken, userValidation.checkPassword, controller.changePassword)

router.get("/getProfile", middleware.authorizationToken, controller.getProfile)

router.patch("/editProfile", middleware.authorizationToken, upload.single("image"), userValidation.checkEditProfile, controller.editProfile)

router.post("/forgotPassword", userValidation.checkForgotPassword, controller.forgotPassword)

router.patch("/resetPassword/:token", userValidation.checkResetPassword, controller.resetPassword)

router.get("/getMyblog", middleware.authorizationToken, controller.getMyBlogs)

module.exports = router    