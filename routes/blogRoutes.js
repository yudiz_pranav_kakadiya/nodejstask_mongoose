const router = require("express").Router()
const controller = require("../controllers/blogController")

const multer = require("multer")
const upload = multer({ dest: "photo/" })

const middleware = require("../middleware/permission")

const validation = require("../utils/validation")


const blogValidation = require("../validation/blogValidation")

router.post("/createBlog", middleware.authorizationToken, upload.single("image"), blogValidation.checkCreateBlog, controller.createBlog)
router.get("/getBlogs", middleware.authorizationToken, controller.getBlog)
router.patch("/editBlogs", middleware.authorizationToken, upload.single("image"), blogValidation.checkEditBlog, controller.editBlog)
router.get("/getAllBlog", middleware.authorizationToken, controller.getAllBlog)

router.get("/", controller.getAllBlog)

router.post("/likes", middleware.authorizationToken, blogValidation.checkLikes, controller.likes)
router.post("/makeBlogUnpublish", middleware.authorizationToken, blogValidation.checkUnpublish, controller.makeBlogUnpublish)
// router.get("/listBlog", middleware.authorizationToken, middleware.roleCheck, controller.listBlog)
module.exports = router    