const router = require("express").Router()
const controller = require("../controllers/adminController")

const multer = require("multer")
const upload = multer({ dest: "photo/" })

// const validation = require("../utils/validation");

const middleware = require("../middleware/permission")


const adminVaidation = require("../validation/adminValidation")

router.post("/createUser", middleware.authorizationToken, middleware.roleCheck, upload.single("image"), adminVaidation.checkSignUp, controller.createUser)
router.get("/listUser", middleware.authorizationToken, middleware.roleCheck, controller.listUser)
router.get("/getProfileOfUser", middleware.authorizationToken, middleware.roleCheck, adminVaidation.checkUserId, controller.getProfileOfUser)
router.get("/deleteUser", middleware.authorizationToken, middleware.roleCheck, adminVaidation.checkUserDelete, controller.deleteUser)
router.get("/search", middleware.authorizationToken, middleware.roleCheck, controller.dashBoard)
module.exports = router    